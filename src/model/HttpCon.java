package model;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

public class HttpCon {
		
	private final String USER_AGENT = "Mozilla/5.0";
				// HTTP GET request
	public String sendGet(City city) throws Exception {

		String url = "https://api.askgeo.com/v1/1726/1e5c6246cd032a95b0e4a0443a10b7d6772a441be036437b72cd00f225677c7b/query.xml?points=" + city.getLatitude() + "%2C" + city.getLongitude() + "&databases=Point%2CTimeZone%2CAstronomy";
//		String url = "http://www.google.com/search?q=mkyong";
			
		
		
		URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();
	
		// optional default is GET
		con.setRequestMethod("GET");
	
		//add request header
		con.setRequestProperty("User-Agent", USER_AGENT);
	
		int responseCode = con.getResponseCode();
		System.out.println("\nSending 'GET' request to URL : " + url);
		System.out.println("Response Code : " + responseCode);
	
		BufferedReader in = new BufferedReader(
		        new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();
	
		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();
	
		//print result
		System.out.println(response.toString());
		
		String s = response.toString();
		
		String substring = s.substring(s.indexOf("CurrentDateTimeIso8601="), s.indexOf("CurrentDateTimeIso8601=") + 50);
		
		return substring;
	}
	
}
