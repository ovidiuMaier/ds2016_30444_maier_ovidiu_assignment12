package model;

import java.util.ArrayList;
import java.util.List;

import dao.UserDao;
import model.User;

public class Authenticator {
	 
	public User authenticate(String email, String password) {	
		
		UserDao userDao = new UserDao();
//		List<User> users = new ArrayList<User>(); 
//		users = userDao.getAllUsers();
//		User user = new User();
//		
//		int found = 0;
//		int i = 0;
//		while(found == 0  && i < users.size() )
//		{
//			user = users.get(i);
//			if (user.getEmail().equals(email) && user.getPassword().equals(password))
//			{
//				found = 1;
//			}
//			i++;
//		}
		
		User user = userDao.getUserByEmail(email);
		int found = 0;
		if(user!= null && user.getPassword().equals(password)) found = 1;
		
		if (user != null && found == 1) {
			return user;
		} else {
			return null;
		}
	}
}
