package model;

import java.util.Date;

import dao.CityDao;

public class Flight {

	private int flightid;
	private int flightNumber;
	private String airplaneType;
	private int departureCityId;
	private String departureTime;
	private int arrivalCityId;
	private String arrivalTime;
	
	public int getFlightid() {
		return flightid;
	}
	public void setFlightid(int flightid) {
		this.flightid = flightid;
	}
	public int getFlightNumber() {
		return flightNumber;
	}
	public void setFlightNumber(int flightNumber) {
		this.flightNumber = flightNumber;
	}
	public String getAirplaneType() {
		return airplaneType;
	}
	public void setAirplaneType(String airplaneType) {
		this.airplaneType = airplaneType;
	}
	public int getDepartureCityId() {
		return departureCityId;
	}
	public void setDepartureCityId(int departureCityId) {
		this.departureCityId = departureCityId;
	}
	public String getDepartureTime() {
		return departureTime;
	}
	public void setDepartureTime(String departureTime) {
		this.departureTime = departureTime;
	}
	public int getArrivalCityId() {
		return arrivalCityId;
	}
	public void setArrivalCityId(int arrivalCityId) {
		this.arrivalCityId = arrivalCityId;
	}
	public String getArrivalTime() {
		return arrivalTime;
	}
	public void setArrivalTime(String arrivalTime) {
		this.arrivalTime = arrivalTime;
	}
	
	public String getDepartureCity() {
		CityDao cityDao = new CityDao();
		City city = new City();
		city = cityDao.getCityById(this.departureCityId);
		String cityName = city.getName();
		return cityName;
	}
	
	public String getArrivalCity() {
		CityDao cityDao = new CityDao();
		City city = new City();
		city = cityDao.getCityById(this.arrivalCityId);
		String cityName = city.getName();
		return cityName;
	}
	
	@Override
    public String toString() {
        return "Flight [flightid=" + flightid + ", flightNumber=" + flightNumber
                + ", airplaneType=" + airplaneType + ", departureTime=" + departureTime + ", arrivalTime=" + arrivalTime + ", departureCityId=" + departureCityId + ", arrivalCityId=" + arrivalCityId + "]";
    }
	
}
