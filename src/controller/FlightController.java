package controller;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.FlightDao;
import model.Flight;

public class FlightController extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private static String INSERT_OR_EDIT = "/flight.jsp";
    private static String LIST_FLIGHT = "/listFlight.jsp";
    private static String LIST_FLIGHT_ADMIN = "/listFlightAdmin.jsp";
    
    private FlightDao dao;

    public FlightController() {
        super();
        dao = new FlightDao();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String forward="";
        String action = request.getParameter("action");

        if (action.equalsIgnoreCase("delete")){
            int flightId = Integer.parseInt(request.getParameter("flightId"));
            dao.deleteFlight(flightId);
            forward = LIST_FLIGHT_ADMIN;
            request.setAttribute("flights", dao.getAllFlights());    
        } else if (action.equalsIgnoreCase("edit")){
            forward = INSERT_OR_EDIT;
            int flightId = Integer.parseInt(request.getParameter("flightId"));
            Flight flight = dao.getFlightById(flightId);
            request.setAttribute("flight", flight);
        } else if (action.equalsIgnoreCase("listFlight")){
            forward = LIST_FLIGHT;
            request.setAttribute("flights", dao.getAllFlights());
        } else if (action.equalsIgnoreCase("listFlightAdmin")){
            forward = LIST_FLIGHT_ADMIN;
            request.setAttribute("flights", dao.getAllFlights());
        } else {
            forward = INSERT_OR_EDIT;
        }

        RequestDispatcher view = request.getRequestDispatcher(forward);
        view.forward(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Flight flight = new Flight();
        flight.setFlightNumber(Integer.parseInt(request.getParameter("flightNumber")));
        flight.setAirplaneType(request.getParameter("airplaneType"));
        flight.setDepartureTime(request.getParameter("departureTime"));
        flight.setArrivalTime(request.getParameter("arrivalTime"));
        flight.setDepartureCityId(Integer.parseInt(request.getParameter("departureCityId")));
        flight.setArrivalCityId(Integer.parseInt(request.getParameter("arrivalCityId")));
        
        String flightid = request.getParameter("flightid");
        if(flightid == null || flightid.isEmpty())
        {
            dao.addFlight(flight);
        }
        else
        {
            flight.setFlightid(Integer.parseInt(flightid));
            dao.updateFlight(flight);
        }
        RequestDispatcher view = request.getRequestDispatcher(LIST_FLIGHT_ADMIN);
        request.setAttribute("flights", dao.getAllFlights());
        view.forward(request, response);
    }
}