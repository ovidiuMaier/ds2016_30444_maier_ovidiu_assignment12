package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.CityDao;
import model.City;
import model.HttpCon;


public class TimeController extends HttpServlet {
	
	private CityDao dao;

    public TimeController() {
        super();
        dao = new CityDao();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String forward="";
        String name = request.getParameter("cityName");
        
        City city = dao.getCityByName(name);
        
        HttpCon con = new HttpCon();
        String s = "";
        
        try {
			s = con.sendGet(city);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
        int id = city.getCityid();

        forward = "localTime.jsp";
        request.setAttribute("id", s);
        
        RequestDispatcher view = request.getRequestDispatcher(forward);
        view.forward(request, response);

	}
}
