<%@ page language="java" contentType="text/html; charset=EUC-KR" pageEncoding="EUC-KR"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-KR">
<title>Show All Flights</title>
</head>
<body>
    <table border=1>
        <thead>
            <tr>
                <th>Flight Id</th>
                <th>Flight Number</th>
                <th>Airplane Type</th>
                <th>Departure Time</th>
                <th>Departure City</th>
                <th>Arrival Time</th>
                <th>Arrival City</th>
            </tr>
        </thead>
        <tbody>
            <c:forEach items="${flights}" var="flight">
                <tr>
                    <td><c:out value="${flight.flightid}" /></td>
                    <td><c:out value="${flight.flightNumber}" /></td>
                    <td><c:out value="${flight.airplaneType}" /></td>
                    <td><c:out value="${flight.departureTime}" /></td>
                    <td><c:out value="${flight.getDepartureCity()}" /></td>
                    <td><c:out value="${flight.arrivalTime}" /></td>
                    <td><c:out value="${flight.getArrivalCity()}" /></td>
                </tr>
            </c:forEach>
        </tbody>
    </table>
    
    <form action="TimeController" method="get">
		City: <input type="text" name="cityName" >
		<input type="submit" value="Get Time" >
    </form>
    
    <form action="LogoutServlet" method="post">
	<input type="submit" value="Logout" >
	</form>
</body>
</html>